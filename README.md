# Go API Docker Example

- This just shows a very simplified example of a GO API with Docker setup and Gitlab CI integration to publish to Gitlab Container Registry. This is used for studying [Kubernetes Course by freecodecamp.org](https://www.youtube.com/watch?v=d6WC5n9G_sM) 

## Clone Link

> Docker Images are located in the [Container Registry Tab](https://gitlab.com/cindrmons-cradle/sandbox/go/go-api-docker-example/container_registry/4355404) 

### Latest Version

```
registry.gitlab.com/cindrmons-cradle/sandbox/go/go-api-docker-example:latest
```

## Docker Pull Image Locally

```
docker pull registry.gitlab.com/cindrmons-cradle/sandbox/go/go-api-docker-example:latest
```

## Docker Run

```
docker run -p 4000:4000 -d --name godocker registry.gitlab.com/cindrmons-cradle/sandbox/go/go-api-docker-example:latest
```

- Access the API Endpoint with [http://localhost:4000](http://localhost:4000)

