# syntax=docker/dockerfile:1

FROM golang:1.20.5-alpine3.18

WORKDIR /app

EXPOSE 4000

COPY go.mod ./
COPY go.sum ./

RUN go mod download

COPY *.go ./

RUN ls -ahl

RUN go build -o ./application

CMD [ "./application" ]

