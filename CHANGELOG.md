# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.0] - 2023-07-01

### Added

- Added README.md

### Updated

- Updated Version number to pretend that I just bumped a version up.

### Fixed

- Added line break as there is no line break when hitting API endpoint on cURL.

### TODO

- Figure out how to systematically set version update.

## [1.0.0] - 2023-07-01

### Added

- Added Version Numbering in API Default Endpoint.

