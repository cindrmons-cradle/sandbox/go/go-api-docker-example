package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func main() {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		hostname, err := os.Hostname()
		if err != nil {
			fmt.Fprintln(w, "os.Hostname can't be found...")
		}
		fmt.Fprintf(w, "VERSION 2: Hello from the %s\n", hostname)
	})

	http.ListenAndServe(":4000", r)
}
